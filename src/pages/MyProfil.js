import React, { useState, useEffect } from 'react';
import ProfilData from "../components/ProfilData";
import axios from 'axios';
import localStorage from "localStorage";

import PostList from '../components/PostList';
import Post from '../components/Post';
import FormAddNewPost from '../components/FormAddNewPost';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

const MyProfil = ({setOnline}) =>
{
    const [isDeleted, setIsDeleted] = useState(false);
    const [refreshPost, setRefreshPost] = useState(true);
    const [user, setUser] = useState(
        {
            id : "",
            username : "",
            firstname : "",
            lastname : "",
            email : "",
            confirmed : "",
            blocked : "",
            role : "",
            bio : ""
        }
    );

    useEffect(() => {
        axios.get("http://localhost:1337/users/me", { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) => {                        
            setUser(response.data);
            setRefreshPost(true);
        })
        .catch(err => { console.log(err); }) ;
    }, []);

    const myPostsSortFunction = (posts, sortOptions) => {
        let list = [];
        for(var i = 0; i < posts.length; i++)
            if(posts[i].user !== null && posts[i].user !== undefined && posts[i].user !== NaN)
                if(posts[i].user.id == sortOptions.userId)
                    list.push(<Post key={i} id={posts[i].id} title={posts[i].title} text={posts[i].content} username={posts[i].user.username} user={user} isMine={true} onAction={setRefreshPost} />);
        return list;
    }

    const handleDelete = () =>
    {
        if(window.confirm("Voulez-vous supprimez votre profil (et tout ses posts) ?"))
        {
            axios.get("http://localhost:1337/posts", { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
            .then((response) =>
            {
                for(var i = 0; i < response.data.length; i++)
                    if(response.data[i].user !== null && response.data[i].user !== undefined && response.data[i].user !== NaN)
                        if(response.data[i].user.id == user.id)
                        {
                            axios.delete("http://localhost:1337/posts/" + response.data[i].id, { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
                            .catch(err => { console.log(err); }) ;
                        }
            })
            .then(() => {
                axios.delete("http://localhost:1337/users/" + user.id, { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
                .then(() => {
                    localStorage.removeItem("jwt");
                    setOnline(false);
                    setIsDeleted(true);
                })
            })
            .catch(err => { console.log(err); }) ;
        }
    }

    return (
        <div>
            {
                isDeleted ? <Redirect to='/login'/>
                :
                <div>
                    <h1>Mon profil</h1>
                    <ProfilData username={user.username} firstname={user.firstname} lastname={user.lastname} email={user.email} confirmed={user.confirmed} blocked={user.blocked} role={user.role.name} bio={user.bio} />
                    <h1>Ajouter un post</h1>
                    <FormAddNewPost onPost={setRefreshPost} user={user} />
                    <h1>Mes posts</h1>
                    <PostList sortFunction={myPostsSortFunction} sortOptions={{userId : user.id}} refresh={refreshPost} onRefreshed={setRefreshPost} />
                    <button type="button" onClick={handleDelete}>Supprimer mon profil</button>
                </div>
            }            
        </div>
    );
}

export default MyProfil;