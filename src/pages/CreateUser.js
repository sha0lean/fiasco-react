import React, {useState} from 'react';
import axios from 'axios';

import Input from "../components/Input";

const CreateUser = () =>
{
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [bio, setBio] = useState("");

    const [provider, setProvider] = useState("");
    const [resetPasswordToken, setResetPasswordToken] = useState("");
    const [confirmed, setConfirmed] = useState(false);
    const [blocked, setBlocked] = useState(false);
    const [role, setRole] = useState("");

    function handleSubmit(event) {
        event.preventDefault();
        axios.post("http://localhost:1337/auth/local/register", {
            username : username,
            email : email,
            provider : provider,
            password : password,
            resetPasswordToken : resetPasswordToken,
            confirmed : false,
            blocked : false,
            role : role,
            firstname : firstname,
            lastname : lastname,
            bio : bio
        })
        .then((response) => console.log("Utilisateur crée"))
        .catch(error => console.log("Erreur"));
    }
    
    return (
        <div className="App">
            <form onSubmit={handleSubmit} >
                <table>
                    <tbody>
                        <tr>
                            <td><Input value={username} setValue={setUsername} label="Nom d'utilisateur" /></td>
                        </tr>
                        <tr>
                            <td><Input value={lastname} setValue={setLastname} label="Nom" /></td>
                        </tr>
                        <tr>
                            <td><Input value={firstname} setValue={setFirstname} label="Prénom" /></td>
                        </tr>
                        <tr>
                            <td><Input value={email} setValue={setEmail} label="Adresse e-mail" /></td>
                        </tr>
                        <tr>
                            <td><Input value={password} setValue={setPassword} label="Mot de passe" /></td>
                        </tr>
                        <tr>
                            <td><Input value={bio} setValue={setBio} label="Biographie" /></td>
                        </tr>
                    </tbody>
                </table>
                <input onClick={handleSubmit} type="submit" value="S'inscrire" />
            </form>
        </div>
    );
};

export default CreateUser;