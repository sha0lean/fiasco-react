import React, { useState, useEffect } from 'react';
import axios from 'axios';
import localStorage from "localStorage";

import Input from "../components/Input";

const Profil = (props) =>
{
    const [id, setId] = useState(null);
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [bio, setBio] = useState("");

    const [provider, setProvider] = useState("");
    const [resetPasswordToken, setResetPasswordToken] = useState("");
    const [confirmed, setConfirmed] = useState(false);
    const [blocked, setBlocked] = useState(false);
    const [role, setRole] = useState("");

    useEffect(() => {
        axios.get("http://localhost:1337/users/me", { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) =>
        {
            setUsername(response.data.username);
            setEmail(response.data.email);
            setFirstname(response.data.firstname);
            setBio(response.data.bio);
            setLastname(response.data.lastname);
            setId(response.data.id);
        });
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.put("http://localhost:1337/users/" + id,        
        {
            firstname : firstname,
            lastname : lastname,
            bio : bio
        },
        {
            headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`},
        }
        ).then((response) => { console.log(response);}
        ).catch(err => { console.log(err); });
    }

    return (
        <div className="profil">
        {blocked ? <span className="blocked">Ce compte est bloqué</span> : null}
        <form onSubmit={handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <td>Pseudonyme</td>
                        <td>{username}</td>
                    </tr>
                    <tr>
                        <td>Adresse mail</td>
                        <td>{email} {confirmed ? <span className="confirmed">(vérifiée)</span> : <span className="unconfirmed">(non vérifée)</span>}</td>
                    </tr>
                    <tr>                
                        <td>Prénom</td>
                        <td><Input value={firstname} setValue={setFirstname} label="Prénom" /></td>
                    </tr>
                    <tr>
                        <td>Nom</td>
                        <td><Input value={lastname} setValue={setLastname} label="Nom" /></td>
                    </tr>
                    <tr>
                        <td>Biographie</td>
                        <td><Input value={bio} setValue={setBio} label="Biographie" /></td>
                    </tr>
                </tbody>
            </table>
            <button type="submit">Confirmer</button>
        </form>
    </div>
    );
}

export default Profil;