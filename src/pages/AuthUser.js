import React, {useState} from 'react';
import Input from "../components/Input";
import axios from 'axios';
import localStorage from "localStorage";
import { useHistory } from "react-router-dom";

const AuthUser = ({setJwt, setOnline}) => {

    const [id, setId]   = useState("");
    const [pwd, setPwd] = useState("");
    const history = useHistory();

    function handleSubmit(event)
    {
        event.preventDefault();
        axios.post("http://localhost:1337/auth/local", {
            identifier: id,
            password: pwd,
        }).then((response) => {
            localStorage.setItem("jwt", response.data.jwt);
            setJwt(response.data.jwt);
            setOnline(true);
            history.push('/profil');
        }).catch(error => console.log(error));
    }
    
    return (
        <div className="App">
            <form onSubmit={handleSubmit} >
                <Input value={id} setValue={setId} label="Id" />
                <Input value={pwd} setValue={setPwd} label="Pwd" />
                <input type="submit" value="Envoyer" />
            </form>
        </div>
    );
};

export default AuthUser;