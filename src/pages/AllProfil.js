import React, { useState, useEffect } from 'react';
import axios from 'axios';
import localStorage from "localStorage";

import MinimalProfil from "../components/MinimalProfil.js";
import Profil from "../components/Profil.js"

const AllProfil = (props) =>
{
    const [refreshPost, setRefreshPost] = useState(true);
    const [list, setList] = useState([]);
    const [displayedUser, setDisplayedUser] = useState(0);

    const displayAllUsers = (users) => {
        let localList = [];
        for(var i = 0; i < users.length; i++)
            localList.push(<MinimalProfil key={i} username={users[i].username} firstname={users[i].firstname} lastname={users[i].lastname} id={users[i].id} setDisplayedUser={setDisplayedUser} />);
        setList(localList);
    }

    useEffect(() => {
        axios.get("http://localhost:1337/users", { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) =>
        {                     
            displayAllUsers(response.data);
        });
    }, []);

    return displayedUser == 0 ? <div>{list}</div> : <Profil id={displayedUser} setDisplayedUser={setDisplayedUser} />;
}

export default AllProfil;