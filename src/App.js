import React, {useState, useEffect} from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import axios from 'axios';
import localStorage from "localStorage";

import AuthUser from './pages/AuthUser.js';
import MyProfil from './pages/MyProfil.js';
import CreateUser from './pages/CreateUser.js';
import EditProfil from './pages/EditProfil.js';
import AllProfil from "./pages/AllProfil.js"


const App = () => {

    const [jwt, setJwt ] = useState('');
    const [online, setOnline] = useState(false);

    const [user, setUser ] = useState([]);
    const [error, setError ] = useState();
    const [users, setUsers ] = useState([]);
    const [username, setUsername ] = useState('');
    const [password, setPassword ] = useState('');

    useEffect(() =>
    {
        let localJwt = localStorage.getItem("jwt");
        if(localJwt != null)
        {
            setJwt(localStorage);
            setOnline(true);
        }
    }, []);

    const handleLogout = () =>
    {
        localStorage.removeItem("jwt");
        setOnline(false);
        setJwt('');
    }

  return (
        <div>
            <Router>
                <div>
                    <ul>
                        <li>
                            { online ? <a href="/" onClick={() => { handleLogout(); }}>Déconnexion</a> : <Link to="/login">Connexion</Link> }
                        </li>
                        <li>
                            <Link to="/signUp">Inscription</Link>
                        </li>
                        <li>
                            <Link to="/profil">Afficher mon profil</Link> 
                        </li>
                        <li>
                            <Link to="/editProfil">Éditer mon profil</Link>
                        </li>
                        <li>
                            <Link to="/allProfil">Liste des utilisateurs</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route path="/profil" render={(props) => { return jwt != '' ? <MyProfil {...props} setOnline={setOnline} /> : <Redirect to="/login" /> }} />
                        <Route path="/login" render={(props) => { return jwt == '' ? <AuthUser {...props} setJwt={setJwt} setOnline={setOnline} /> : <MyProfil {...props}  setOnline={setOnline} />}} />
                        <Route path="/signUp" render={(props) => <CreateUser {...props} jwt={jwt} />}/ >
                        <Route path="/editProfil" render={(props) => { return jwt != '' ? <EditProfil {...props} /> : <Redirect to="/login" /> }} />
                        <Route path="/allProfil" render={(props) => <AllProfil {...props} />}/ >
                    </Switch>
                </div>
        </Router>
    </div>
    );
}

export default App; 