import React, { useState, useEffect } from 'react';
import ProfilData from "./ProfilData";
import axios from 'axios';
import localStorage from "localStorage";

import PostList from './PostList';
import Post from './Post';

const Profil = ({id, setDisplayedUser}) =>
{
    const [content, setContent] = useState([]);  

    useEffect(() => {
        axios.get("http://localhost:1337/users/" + id, { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) =>
        {
            setContent(
            <div>
                <button type="button" onClick={() => {setDisplayedUser(0)}}>Retour</button>
                <h1>{response.data.username}</h1>
                <ProfilData username={response.data.username} firstname={response.data.firstname} lastname={response.data.lastname} email={response.data.email} confirmed={response.data.confirmed} blocked={response.data.blocked} role={response.data.role.name} bio={response.data.bio} />
                <h1>Posts</h1>        
                <PostList sortFunction={myPostsSortFunction} sortOptions={{userId : id}}/>
            </div> 
            );
        });
    }, []);

    const myPostsSortFunction = (posts, sortOptions) => {
        let list = [];
        for(var i = 0; i < posts.length; i++)
            if(posts[i].user !== null && posts[i].user !== undefined && posts[i].user !== NaN)
                if(posts[i].user.id == sortOptions.userId)
                    list.push(<Post key={i} title={posts[i].title} text={posts[i].content} username={posts[i].user.username}/>);
        return list;
    }

    return (
        <div>{content}</div>
    );
}

export default Profil;