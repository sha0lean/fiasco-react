import React from 'react';

const MinimalProfil = ({username, firstname, lastname, id, setDisplayedUser}) =>
{
    const handleClick = () =>
    {
        setDisplayedUser(id);
    }

    return (
        <div className="profil" style={{border : '1px solid black', display : 'inline-block', height : 100}}>
            <table>
                <tbody>
                    <tr>
                        <td>{username}</td>
                    </tr>
                    <tr>
                        <td>{firstname}</td>
                    </tr>
                    <tr>
                        <td>{lastname}</td>
                    </tr>
                    <tr>
                        <td><button type="button" onClick={() => {handleClick(id)}}>visiter</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default MinimalProfil;