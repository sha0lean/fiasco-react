import axios from 'axios';
import React, { useState, useEffect } from 'react';

import Input from "./Input";

function Post({id, title, text, username, user = null, isMine = false, onAction = null})
{
    const [isEditing, setIsEditing] = useState(false);
    const [localTitle, setLocalTitle] =  useState(title);
    const [localText, setLocalText] =  useState(text);

    const onEdit = (id) =>
    {
        axios.put("http://localhost:1337/posts/" + id,
        {
            title : localTitle,
            content : localText,
            user : user
        },
        { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) =>
        {
            setIsEditing(false);
            onAction(true);
        })
        .catch(err => { console.log(err); }) ;
    }

    const onDelete = (id) =>
    {
        axios.delete("http://localhost:1337/posts/" + id, { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) => {
            onAction(true);
        })
        .catch(err => { console.log(err); }) ;
    }

    return (
        <div style={{border : '1px solid black'}}>
            <h4>{username}</h4>     
            {isEditing ?
                <table>
                    <tbody>
                        <tr>
                            <td>Titre</td>
                            <td><Input value={localTitle} setValue={setLocalTitle} label="Titre" /></td>
                        </tr>
                        <tr>
                            <td>Texte</td>
                            <td><Input value={localText} setValue={setLocalText} label="Texte" /></td>
                        </tr>
                    </tbody>
                </table>
                :                
                <div>  
                    <h2>{localTitle}</h2>
                    <p>{localText}</p>
                </div>
            }

            {isMine ?
            <div>
                {isEditing ?
                    <div>
                        <button type="button" onClick={() => {onEdit(id);}}>Confirmer</button>
                        <button type="button" onClick={() => {
                            setIsEditing(false);
                            setLocalTitle(title);
                            setLocalText(text);
                        }}>Annuler</button>
                    </div>
                    :
                    <button type="button" onClick={() => {setIsEditing(true);}}>Modifier</button>
                }
                
                <button type="button" onClick={() => {onDelete(id);}}>Supprimer</button>
            </div>
            : null }
        </div>
    );
};

export default Post;