import React from 'react';

const Input = ({value, setValue, label, error = false}) => (
    <input value={value} onChange={(e) => setValue(e.target.value)} placeholder={label} style={{borderColor: error ? 'red' : 'black'}} />
);

export default Input;