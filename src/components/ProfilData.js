import React from 'react';

const ProfilData = ({username, firstname, lastname, email, confirmed, blocked, role, bio}) =>
{
    return (
        <div className="profil">
            {blocked ? <span className="blocked">Ce compte est bloqué</span> : null}
            <table>
                <tbody>
                    <tr>
                        <td>Pseudonyme</td>
                        <td>{username}</td>
                    </tr>
                    <tr>
                        <td>Prénom</td>
                        <td>{firstname}</td>
                    </tr>
                    <tr>
                        <td>Nom</td>
                        <td>{lastname}</td>
                    </tr>
                    <tr>
                        <td>Adresse mail</td>
                        <td>{email} {confirmed ? <span className="confirmed">(vérifiée)</span> : <span className="unconfirmed">(non vérifée)</span>}</td>
                    </tr>
                    <tr>
                        <td>Pseudonyme</td>
                        <td>{username}</td>
                    </tr>
                    <tr>
                        <td>Role</td>
                        <td>{role}</td>
                    </tr>
                    <tr>
                        <td>Biographie</td>
                        <td>{bio}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default ProfilData;