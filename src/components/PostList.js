import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Post from "./Post";
import localStorage from "localStorage";
import { createPortal } from 'react-dom';

const PostList = ({sortFunction, sortOptions, refresh = true, onRefreshed = null }) =>
{
    const [posts, setPosts] = useState([]);

    useEffect(() =>
    {
        if(refresh)
        {
            axios.get("http://localhost:1337", { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
            .then((response) => {
                setPosts(sortFunction(response.data, sortOptions));
            })
            .catch(err => { console.log(err); }) ;
            if(onRefreshed != null)
                onRefreshed(false);
        }
    }, [sortOptions]);

    return (
        <div>
            {posts}
        </div>
    );
};

export default PostList;