import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Input from './Input'

const FormAddNewPost = ({onPost, user}) => {
    
    const [newPostTitle, setNewPostTitle] = useState('');
    const [newPostContent, setNewPostContent] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        axios.post("http://localhost:1337/posts",
        {
            title : newPostTitle,
            content : newPostContent,
            user : user
        },
        { headers : {Authorization: `Bearer ${localStorage.getItem("jwt")}`} })
        .then((response) =>
        {
            onPost(true);
            setNewPostTitle('');
            setNewPostContent('');
        })
        .catch(err => { console.log(err); }) ;
    }

    return (
        <form onSubmit={handleSubmit}>
            <table>
                <tbody>
                    <tr>
                        <td>Titre</td>
                        <td><Input value={newPostTitle} setValue={setNewPostTitle} label={"Titre"} /></td>
                    </tr>
                    <tr>
                        <td>Texte</td>
                        <td><textarea placeholder={"Saisissez votre texte..."} value={newPostContent} onChange={(e) => setNewPostContent(e.target.value)} rows={10} cols={50}></textarea></td>
                    </tr>
                </tbody>
            </table>
            <button onSubmit={handleSubmit} type="submit">Poster</button>
        </form>
    );
};

export default FormAddNewPost;